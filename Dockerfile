# from the please-openit/markdown-to-pdf-CI project
# https://github.com/please-openit/markdown-to-pdf-CI/blob/master/docker/Dockerfile
FROM node:12-buster

RUN apt-get update
RUN apt-get -y install chromium
RUN npm install -g mdpdf --unsafe-perm=true --allow-root
RUN npm install -g markdown-toc

RUN mkdir /data
WORKDIR /data
